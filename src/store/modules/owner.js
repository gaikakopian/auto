import json from '../json/data.json'

const initialState = {
    owners: [],
};

const getters = {
    getOwner: state => key => state.owners[key],
    getOwners: state => state.owners
};

const mutations = {
    updateOwner: (state, body) => {
        state.owners.splice(body.key, 0);
        state.owners[body.key] = body.owner;
        window.localStorage.setItem('owners', JSON.stringify(state.owners));
    },
    updateOwners: (state, owners) => {
        const storedOwners = JSON.parse(window.localStorage.getItem('owners'));

        if(!storedOwners){
            state.owners = owners;
            window.localStorage.setItem('owners', JSON.stringify(owners));
        }else{
            state.owners = storedOwners;
        }
    },
    createOwner: (state, owner) => {
        let body = owner;
        body.id = state.owners.length + 1;
        state.owners.push(body);
        window.localStorage.setItem('owners', JSON.stringify(state.owners));
    },
};

const actions = {
    async fetchOwners({ commit }) {
        commit('updateOwners', json.carOwners);
    },
    async fetchCreateOwner({ commit }, body) {
        commit('createOwner', body);
    },
    async fetchUpdateOwner({ commit }, body) {
        commit('updateOwner', body);
    },
};


export default {
    namespaced: true,
    state: initialState,
    getters,
    mutations,
    actions,
};

