import json from '../json/data.json'
import moment from 'moment'

const initialState = {
    arendas: [],
};

const getters = {
    getArenda: state => key => state.arendas[key],
    getArendas: state => state.arendas
};

const mutations = {
    updateArenda: (state, body) => {
        state.arendas.splice(body.key, 0);
        state.arendas[body.key] = body.arenda;
        window.localStorage.setItem('arendas', JSON.stringify(state.arendas));
    },
    updateArendas: (state, arendas) => {
        const storedArendas = JSON.parse(window.localStorage.getItem('arendas'));

        if(!storedArendas){
            state.arendas = arendas;
            window.localStorage.setItem('arendas', JSON.stringify(arendas));
        }else{
            state.arendas = storedArendas;
        }

        state.arendas.forEach(function(element,index) {
            if(moment(Date.now()).format("YYYY-MM-DD HH:mm:ss") > element.to && element.status != 'started'){
                state.arendas[index].status = 'started';
            }
        });
    },
    createArenda: (state, arenda) => {
        let body = arenda;
        body.id = state.arendas.length + 1;
        state.arendas.push(body);
        window.localStorage.setItem('arendas', JSON.stringify(state.arendas));
    },
};

const actions = {
    async fetchArendas({ commit }) {
        commit('updateArendas', json.arendas);
    },
    async fetchCreateArenda({ commit }, body) {
        commit('createArenda', body);
    },
    async fetchUpdateArenda({ commit }, body) {
        commit('updateArenda', body);
    },
};


export default {
    namespaced: true,
    state: initialState,
    getters,
    mutations,
    actions,
};

