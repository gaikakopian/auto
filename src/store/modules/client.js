import json from '../json/data.json'

const initialState = {
    clients: [],
};

const getters = {
    getClient: state => key => state.clients[key],
    getClients: state => state.clients
};

const mutations = {
    updateClient: (state, body) => {
        state.clients.splice(body.key, 0);
        state.clients[body.key] = body.client;
        window.localStorage.setItem('clients', JSON.stringify(state.clients));
    },
    updateClients: (state, clients) => {
        const storedClients = JSON.parse(window.localStorage.getItem('clients'));

        if(!storedClients){
            state.owners = clients;
            window.localStorage.setItem('clients', JSON.stringify(clients));
        }else{
            state.clients = storedClients;
        }
    },
    createClient: (state, client) => {
        let body = client;
        body.id = state.clients.length + 1;
        state.clients.push(body);
        window.localStorage.setItem('clients', JSON.stringify(state.clients));
    },
    deleteClient: (state, index) => {
        state.clients.splice(index, 1);
        window.localStorage.setItem('clients', JSON.stringify(state.clients));
    },
};

const actions = {
    async fetchClients({ commit }) {
        commit('updateClients', json.clients);
    },
    async fetchCreateClient({ commit }, body) {
        commit('createClient', body);
    },
    async fetchUpdateClient({ commit }, body) {
        commit('updateClient', body);
    },
    async fetchDeleteClient({ commit }, index) {
        commit('deleteClient', index);
    },
};


export default {
    namespaced: true,
    state: initialState,
    getters,
    mutations,
    actions,
};

