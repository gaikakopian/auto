import json from '../json/data.json'

const initialState = {
    cars: [],
};

const getters = {
    getCar: state => key => state.cars[key],
    getCars: state => state.cars
};

const mutations = {
    updateCar: (state, body) => {
        state.cars[body.key] = body.car;
        window.localStorage.setItem('cars', JSON.stringify(state.cars));
    },
    deleteCar: (state, index) => {
        state.cars.splice(index, 1);
        window.localStorage.setItem('cars', JSON.stringify(state.cars));
    },
    updateCars: (state, cars) => {
        const storedCars = JSON.parse(window.localStorage.getItem('cars'));
        if(!storedCars){
            state.cars = cars;
            window.localStorage.setItem('cars', JSON.stringify(cars));
        }else{
            state.cars = storedCars;
        }
    },
    createCar: (state, car) => {
        let body = car;
        body.id = state.cars.length + 1;
        state.cars.push(body);
        window.localStorage.setItem('cars', JSON.stringify(state.cars));
    },
};

const actions = {
    async fetchCars({ commit }) {
        commit('updateCars', json.cars);
    },
    async fetchCreateCar({ commit }, body) {
        commit('createCar', body);
    },
    async fetchUpdateCar({ commit }, body) {
        commit('updateCar', body);
    },
    async fetchDeleteCar({ commit }, index) {
        commit('deleteCar', index);
    },
};


export default {
    namespaced: true,
    state: initialState,
    getters,
    mutations,
    actions,
};

