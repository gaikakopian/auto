import owner from './owner';
import car from './car';
import client from './client';
import arenda from './arenda';

export default {
  namespaced: true,
  modules: {
      owner,
      car,
      client,
      arenda
  },
};
