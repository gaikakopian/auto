import Vue from 'vue';
import App from './components/App.vue';
import Vuetify from 'vuetify';
import router from './router';
import store from './store';
import './plugins'

import 'vuetify/dist/vuetify.min.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.use(Vuetify, {
    iconfont: 'fa'
});

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app')
