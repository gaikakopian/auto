import MainLayout from '@/layouts/MainLayout';

const TableContainer = () => import('./containers/TableContainer');
const CreateContainer = () => import('./containers/CreateContainer');

export default {
  path: '/cars',
  component: MainLayout,
  children: [
    {
      path: '',
      name: 'cars.list',
      component: TableContainer,
    },
    {
        path: '/cars/create',
        name: 'cars.create',
        component: CreateContainer,
    },
    {
        path: '/cars/update/:id',
        name: 'cars.update',
        component: CreateContainer,
    },
  ],
};
