import MainLayout from '@/layouts/MainLayout';

const TableContainer = () => import('./containers/TableContainer');
const CreateContainer = () => import('./containers/CreateContainer');

export default {
  path: '/owners',
  component: MainLayout,
  children: [
    {
      path: '',
      name: 'owners.list',
      component: TableContainer,
    },
    {
        path: '/owners/create',
        name: 'owners.create',
        component: CreateContainer,
    },
    {
        path: '/owners/update/:id',
        name: 'owners.update',
        component: CreateContainer,
    },
  ],
};
