import MainLayout from '@/layouts/MainLayout';

const TableContainer = () => import('./containers/TableContainer');
const CreateContainer = () => import('./containers/CreateContainer');

export default {
  path: '/rents',
  component: MainLayout,
  children: [
    {
      path: '',
      name: 'arendas.list',
      component: TableContainer,
    },
    {
        path: '/rents/create',
        name: 'arendas.create',
        component: CreateContainer,
    },
    {
        path: '/rents/update/:id',
        name: 'arendas.update',
        component: CreateContainer,
    },
  ],
};
