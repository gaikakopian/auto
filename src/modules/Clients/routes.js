import MainLayout from '@/layouts/MainLayout';

const TableContainer = () => import('./containers/TableContainer');
const CreateContainer = () => import('./containers/CreateContainer');

export default {
  path: '/clients',
  component: MainLayout,
  children: [
    {
      path: '',
      name: 'clients.list',
      component: TableContainer,
    },
    {
        path: '/clients/create',
        name: 'clients.create',
        component: CreateContainer,
    },
    {
        path: '/clients/update/:id',
        name: 'clients.update',
        component: CreateContainer,
    },
  ],
};
