/**
 * Not used;
 * please use
 * articles.js
 * @type {{}}
 */
const initialState = {

};

const getters = {

};

const actions = {

};

const mutations = {

};


export default {
  namespaced: true,
  state: initialState,
  getters,
  actions,
  mutations,
};
