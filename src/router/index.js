import Vue from 'vue';
import Router from 'vue-router';

import { routes as CarOwners } from '@/modules/CarOwners';
import { routes as Cars } from '@/modules/Cars';
import { routes as Clients } from '@/modules/Clients';
import { routes as Arendas } from '@/modules/Arendas';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/owners',
    },
  ],
});

router.addRoutes([
    CarOwners,
    Cars,
    Clients,
    Arendas
]);

export default router;
